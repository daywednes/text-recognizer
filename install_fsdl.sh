#Run on ec2 machine
#set folder path
export FSDLPATH = ~/text-recognizer/fsdl_code

#Install lfs for git (because some file is stored in lfs mode)
#Importance! 
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash;
sudo apt-get install git-lfs;

#Clone full-stack-deep-learning project:
#Importance! 
cd ~;
git clone https://gitlab.com/daywednes/text-recognizer.git ;

#install environment
cd FSDLPATH/;
pip install -r requirements.txt -r requirements-dev.txt;
export PYTHONPATH=.;


#Get data set
cd FSDLPATH/lab1;
python text_recognizer/datasets/emnist_dataset.py;

#install new cudnn lib
conda install cudnn;

#Lib for parallel
sudo apt install moreutils;
sudo apt install parallel;