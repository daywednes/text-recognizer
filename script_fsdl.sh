#Switch version to 10.2 Because the code of fsdl use cuDNN 7.6.4 but the defauld version of AWS is cuDNN 7.5.1
#and add PYTHONPATH
#Importance! 
export PATH=$PATH:/usr/local/cuda-10.2/bin;
export CUDADIR=/usr/local/cuda-10.2;
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-10.2/lib64;
export FSDLPATH = ~/text-recognizer/fsdl_code

cd FSDLPATH;
export PYTHONPATH=. ;



#Train data lab 1
cd FSDLPATH/lab1;
training/run_experiment.py --save '{"dataset": "EmnistDataset", "model": "CharacterModel", "network": "mlp",  "train_args": {"batch_size": 256}}' 2>&1 | tee ~/lab1_1.log ;

#Predict character lab 1
pytest -s text_recognizer/tests/test_character_predictor.py 2>&1 | tee ~/lab1_2.log ;

#train data lab 2
cd FSDLPATH/lab2;
training/run_experiment.py '{"dataset": "EmnistDataset", "model": "CharacterModel", "network": "lenet", "train_args": {"epochs": 1}}' 2>&1 | tee ~/lab2_1.log ;
#train data lab 2 with subsample
training/run_experiment.py '{"dataset": "EmnistDataset", "dataset_args": {"subsample_fraction": 0.25}, "model": "CharacterModel", "network": "lenet"}' 2>&1 | tee ~/lab2_2.log ;

#Training Reading multiple characters at once
python training/run_experiment.py --save '{"train_args": {"epochs": 5}, "dataset": "EmnistLinesDataset", "dataset_args": {"max_length": 8, "max_overlap": 0}, "model": "LineModel", "network": "line_cnn_all_conv"}' 2>&1 | tee ~/lab2_3.log; 

#train data lab 3
cd FSDLPATH/lab3;
python training/run_experiment.py --save '{"train_args": {"epochs": 16}, "dataset": "EmnistLinesDataset", "model": "LineModelCtc", "network": "line_lstm_ctc"}' 2>&1 | tee ~/lab3_1.log;

#train data lab 4
cd FSDLPATH/lab4;
python training/run_experiment.py --save '{"dataset": "IamLinesDataset", "model": "LineModelCtc", "network": "line_lstm_ctc"}' 2>&1 | tee ~/lab4_1.log;
#Run multi experiemnt at once
tasks/prepare_sample_experiments.sh;
tasks/prepare_sample_experiments.sh | parallel -j2;

#The next step to use wandb sweep but sweep.yaml file is missed. 
#Because lack of time, keep moving to lab 5.

#Lab 5: get iam data set
cd FSDLPATH/lab5;
python text_recognizer/datasets/iam_dataset.py;

#Check raw data in: FSDLPATH/data/raw/iam/iamdb/forms
#Check notebooks/04-look-at-iam-paragraphs.ipynb and notebooks/04b-look-at-line-detector-predictions.ipynb

pytest -s text_recognizer/tests/test_paragraph_text_recognizer.py;

#Lab 6: no script
#Lab 7: Run auto test
cd FSDLPATH/lab7;
#Importance! edit tasks/lint.sh, replace line 8: safety check -r ../requirements.txt -r ../requirements-dev.txt || FAILURE=true
tasks/lint.sh;

#Lab 8: Run web app server
cd FSDLPATH/lab8;
python api/app.py;

#Open new terminal
#Test local file
cd FSDLPATH/lab8;
export API_URL=http://0.0.0.0:8000

curl -X POST "${API_URL}/v1/predict" -H 'Content-Type: application/json' --data '{ "image": "data:image/png;base64,'$(base64 -w0 -i text_recognizer/tests/support/emnist_lines/or\ if\ used\ the\ results.png)'" }'
curl -X POST "${API_URL}/v1/predict" -H 'Content-Type: application/json' --data '{ "image": "data:image/png;base64,'$(base64 -w0 -i text_recognizer/tests/support/emnist_lines/do\ that\ In.png)'" }'
curl -X POST "${API_URL}/v1/predict" -H 'Content-Type: application/json' --data '{ "image": "data:image/png;base64,'$(base64 -w0 -i text_recognizer/tests/support/emnist_lines/Corsi\ left\ for.png)'" }'

#The result is very bad when test with the real hand writing line
curl -X POST "${API_URL}/v1/predict" -H 'Content-Type: application/json' --data '{ "image": "data:image/png;base64,'$(base64 -w0 -i text_recognizer/tests/support/iam_lines/and\ came\ into\ the\ livingroom,\ where.png)'" }'
curl -X POST "${API_URL}/v1/predict" -H 'Content-Type: application/json' --data '{ "image": "data:image/png;base64,'$(base64 -w0 -i text_recognizer/tests/support/iam_lines/He\ rose\ from\ his\ breakfast-nook\ bench.png)'" }'

#Test file on outside server
curl "${API_URL}/v1/predict?image_url=http://s3-us-west-2.amazonaws.com/fsdl-public-assets/emnist_lines/or%2Bif%2Bused%2Bthe%2Bresults.png"

#Unit test for web app
tasks/test_api.sh

#Build docker file
#Importance! edit tasks/build_api_docker.sh, replace line 3: sed 's/tensorflow==/tensorflow-cpu==/' ../requirements.txt > api/requirements.txt
tasks/build_api_docker.sh
#Run docker file
docker run -p 8000:8000 --name api -it --rm text_recognizer_api

#Check in web browser (Change IP address to the current ip address)
#link: http://54.183.155.219:8000/v1/predict?image_url=http://s3-us-west-2.amazonaws.com/fsdl-public-assets/emnist_lines/or%2Bif%2Bused%2Bthe%2Bresults.png

#####################################################
#Push container to docker hub
#Login 
docker login --username=anhhuy0501 #input password

#Check image name
docker image ls

#Rename container (replace IMAGEID!)
docker tag IMAGEID anhhuy0501/text_recognizer_api

#push container
docker push anhhuy0501/text_recognizer_api

######################################################
#New machine:
#Login
docker login --username=anhhuy0501 #input password

#Get and run container
docker run -p 8000:8000 --name pullapi -it --rm anhhuy0501/text_recognizer_api

#Check in web browser (Change IP address to the current webserver ip address)
#link: http://54.183.155.219:8000/v1/predict?image_url=http://s3-us-west-2.amazonaws.com/fsdl-public-assets/emnist_lines/or%2Bif%2Bused%2Bthe%2Bresults.png
#or local: http://localhost:8000/v1/predict?image_url=http://s3-us-west-2.amazonaws.com/fsdl-public-assets/emnist_lines/or%2Bif%2Bused%2Bthe%2Bresults.png
#start jupyter lab
cd ~;
jupyter lab --port 8888 --allow-root --no-browser --ip=0.0.0.0 2>&1 | tee ~/jupyter_output.log ;

