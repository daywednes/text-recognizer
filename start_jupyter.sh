#start jupyter lab
cd ~;
jupyter lab --port 8888 --allow-root --no-browser --ip=0.0.0.0 2>&1 | tee ~/jupyter_output.log ;