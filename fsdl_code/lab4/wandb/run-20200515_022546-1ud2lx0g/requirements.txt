absl-py==0.9.0
alabaster==0.7.12
anaconda-client==1.7.2
anaconda-navigator==1.9.12
anaconda-project==0.8.3
appdirs==1.4.3
argh==0.26.2
argparse==1.4.0
asn1crypto==1.3.0
astroid==2.3.3
astropy==4.0.1.post1
astunparse==1.6.3
atomicwrites==1.3.0
attrs==19.3.0
autopep8==1.4.4
autovizwidget==0.15.0
babel==2.8.0
backcall==0.1.0
backports.functools-lru-cache==1.6.1
backports.shutil-get-terminal-size==1.0.0
backports.tempfile==1.0
backports.weakref==1.0.post1
bandit==1.6.2
beautifulsoup4==4.8.2
bitarray==1.2.1
bkcharts==0.2
black==19.10b0
bleach==3.1.4
blessings==1.7
bokeh==2.0.1
boltons==20.0.0
boto3==1.13.1
boto==2.49.0
botocore==1.16.1
bottleneck==1.3.2
cachetools==4.0.0
certifi==2019.11.28
cffi==1.14.0
chardet==3.0.4
click==7.1.1
cloudpickle==1.3.0
clyent==1.2.2
colorama==0.4.3
conda-build==3.18.11
conda-package-handling==1.6.0
conda-verify==3.4.2
conda==4.8.3
configparser==5.0.0
contextlib2==0.6.0.post1
cryptography==2.8
cycler==0.10.0
cython==0.29.15
cytoolz==0.10.1
dask==2.14.0
decorator==4.4.2
defusedxml==0.6.0
diff-match-patch==20181111
distlib==0.3.0
distributed==2.14.0
docker-pycreds==0.4.0
docutils==0.16
dparse==0.5.0
editdistance==0.5.3
entrypoints==0.3
environment-kernels==1.1.1
et-xmlfile==1.0.1
fastcache==1.1.0
filelock==3.0.12
flake8==3.7.9
flask==1.1.1
fsspec==0.7.1
future==0.18.2
gast==0.3.3
gevent==1.4.0
gitdb==4.0.2
gitpython==3.1.0
glob2==0.7
gmpy2==2.0.8
google-auth-oauthlib==0.4.1
google-auth==1.12.0
google-pasta==0.2.0
gpustat==0.6.0
gql==0.2.0
gradescope-utils==0.3.1
graphql-core==1.1
greenlet==0.4.15
grequests==0.4.0
grpcio==1.27.2
h5py==2.10.0
hdijupyterutils==0.15.0
heapdict==1.0.1
html5lib==1.0.1
hypothesis==5.8.3
idna==2.9
imageio==2.8.0
imagesize==1.2.0
importlib-metadata==1.6.0
intervaltree==3.0.2
ipykernel==5.2.0
ipyparallel==6.2.4
ipython-genutils==0.2.0
ipython==7.13.0
ipywidgets==7.5.1
isort==4.3.21
itermplot==0.331
itsdangerous==1.1.0
jdcal==1.4.1
jedi==0.16.0
jeepney==0.4.3
jinja2==2.11.1
jmespath==0.9.4
joblib==0.14.1
json5==0.9.4
jsonschema==3.2.0
jupyter-client==6.1.2
jupyter-console==6.1.0
jupyter-core==4.6.3
jupyter==1.0.0
jupyterlab-server==1.0.7
jupyterlab==2.0.1
keras-preprocessing==1.1.0
keyring==21.1.1
kiwisolver==1.1.0
lazy-object-proxy==1.4.3
libarchive-c==2.8
lief==0.9.0
llvmlite==0.31.0
locket==0.2.0
lxml==4.5.0
markdown==3.2.1
markupsafe==1.1.1
matplotlib==3.2.1
mccabe==0.6.1
mistune==0.8.4
mkl-fft==1.0.15
mkl-random==1.1.0
mkl-service==2.3.0
mock==4.0.1
more-itertools==8.2.0
mpmath==1.1.0
msgpack==1.0.0
multipledispatch==0.6.0
mypy-extensions==0.4.3
mypy==0.770
navigator-updater==0.2.1
nb-conda-kernels==2.2.3
nb-conda==2.2.1
nbconvert==5.6.1
nbformat==5.0.4
networkx==2.4
nltk==3.4.5
nose==1.3.7
notebook==6.0.3
numba==0.48.0
numexpr==2.7.1
numpy==1.18.2
numpydoc==0.9.2
nvidia-ml-py3==7.352.0
oauthlib==3.1.0
olefile==0.46
opencv-python-headless==4.2.0.32
openpyxl==3.0.3
opt-einsum==3.2.0
packaging==20.3
pandas==1.0.3
pandocfilters==1.4.2
parso==0.6.2
partd==1.1.0
path==13.1.0
pathlib2==2.3.5
pathspec==0.7.0
pathtools==0.1.2
patsy==0.5.1
pbr==5.4.4
pep8==1.7.1
pexpect==4.8.0
pickleshare==0.7.5
pillow==7.0.0
pip==20.0.2
pipenv==2018.11.26
pkginfo==1.5.0.1
plotly==4.6.0
pluggy==0.13.1
ply==3.11
prometheus-client==0.7.1
promise==2.3
prompt-toolkit==3.0.5
protobuf3-to-dict==0.1.5
protobuf==3.11.3
psutil==5.7.0
psycopg2==2.7.5
ptyprocess==0.6.0
py==1.8.1
pyasn1-modules==0.2.8
pyasn1==0.4.8
pycodestyle==2.5.0
pycosat==0.6.3
pycparser==2.20
pycrypto==2.6.1
pycurl==7.43.0.5
pydocstyle==5.0.2
pyflakes==2.1.1
pygal==2.4.0
pygments==2.6.1
pykerberos==1.2.1
pylint==2.4.4
pyodbc==4.0.0-unsupported
pyopenssl==19.1.0
pyparsing==2.4.6
pyrsistent==0.16.0
pysocks==1.7.1
pytest-arraydiff==0.3
pytest-astropy-header==0.1.2
pytest-astropy==0.8.0
pytest-doctestplus==0.5.0
pytest-openfiles==0.4.0
pytest-remotedata==0.3.2
pytest==5.4.1
python-dateutil==2.8.1
python-jsonrpc-server==0.3.4
python-language-server==0.31.9
pytz==2019.3
pywavelets==1.1.1
pyxdg==0.26
pyyaml==5.3.1
pyzmq==19.0.0
qdarkstyle==2.8
qtawesome==0.7.0
qtconsole==4.7.2
qtpy==1.9.0
redis==3.4.1
redlock-py==1.0.8
regex==2020.2.20
requests-kerberos==0.12.0
requests-oauthlib==1.3.0
requests==2.23.0
retrying==1.3.3
rope==0.16.0
rsa==4.0
rtree==0.9.3
ruamel-yaml==0.15.87
s3fs==0.4.0
s3transfer==0.3.3
safety==1.8.7
sagemaker==1.56.1.post1
scikit-image==0.16.2
scikit-learn==0.22.1
scipy==1.4.1
seaborn==0.10.0
secretstorage==3.1.2
send2trash==1.5.0
sentry-sdk==0.14.3
setuptools==46.1.3.post20200330
shortuuid==1.0.1
simplegeneric==0.8.1
singledispatch==3.4.0.3
six==1.14.0
smdebug-rulesconfig==0.1.2
smmap==3.0.1
snowballstemmer==2.0.0
sortedcollections==1.1.2
sortedcontainers==2.1.0
soupsieve==2.0
sparkmagic==0.15.0
sphinx==2.4.4
sphinxcontrib-applehelp==1.0.2
sphinxcontrib-devhelp==1.0.2
sphinxcontrib-htmlhelp==1.0.3
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.3
sphinxcontrib-serializinghtml==1.1.4
sphinxcontrib-websupport==1.2.1
spyder-kernels==1.9.0
spyder==4.1.2
sqlalchemy==1.3.15
statsmodels==0.11.0
stevedore==1.32.0
subprocess32==3.5.4
sympy==1.5.1
tables==3.6.1
tblib==1.6.0
tensorboard-plugin-wit==1.6.0.post2
tensorboard==2.2.0
tensorflow-estimator==2.2.0rc0
tensorflow==2.2.0rc2
termcolor==1.1.0
terminado==0.8.3
testpath==0.4.4
toml==0.10.0
toolz==0.10.0
tornado==6.0.4
tqdm==4.44.1
traitlets==4.3.3
typed-ast==1.4.1
typing-extensions==3.7.4.1
ujson==1.35
unicodecsv==0.14.1
urllib3==1.25.8
virtualenv-clone==0.5.4
virtualenv==20.0.15
wandb==0.8.36
watchdog==0.10.2
wcwidth==0.1.9
webencodings==0.5.1
werkzeug==1.0.0
wheel==0.34.2
widgetsnbextension==3.5.1
wrapt==1.11.2
wurlitzer==2.0.0
xlrd==1.2.0
xlsxwriter==1.2.8
xlwt==1.3.0
xmltodict==0.12.0
yapf==0.28.0
zict==2.0.0
zipp==3.1.0